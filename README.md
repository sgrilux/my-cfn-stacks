## cfn/vpc-public-private-single-az.yaml

    It creates:
    -   a VPC
	-   a Public Subnet in the first AZ
	-   a Private Subnet in the first AZ
	-   an IGW for the Public Subnet
	-   a NAT-GW for the Private Subnet
	-   NACL for both subnets
	-   Public and Private Route tables

## cfn/bastion.yaml

    It creates:
   	-   An EC2 instance to be used as bastion host
	-   SG for the bastion
    	-    It allows only SSH connections

## cfn-deploy.sh

It's just an helper script to deploy stacks.