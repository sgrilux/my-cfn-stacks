#!/usr/bin/env bash
# Helper script to interact with cloudformation through the AWS-CLI

function usage() {
    cat << __EOF__
    Usage:
      $0 -c <command> -f <stack-file> -n <stack-name>

      where:
       <command>: [create, update, delete, validate]
       <stack-file>: is the file that containt the CFN stack
       <stack-name>: the name to give to the stack
    e.g.
      $0 -c create -f cfn/base.yaml -n CFN-Deploy-Base
__EOF__
    exit 1
}

REGION="eu-west-1"

while getopts ":hc:f:n:" opt; do
    case "${opt}" in
        c)
            CMD=${OPTARG}
            ;;
        f)
            STACK=${OPTARG}
            ;;
        n)
            STACK_NAME=${OPTARG}
            ;;
        h|*)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

case ${CMD} in
    create|update)
        # Test if the file containing the CFN templates exists
        [[ ! -f ${STACK} ]] && echo "File not found" && exit 1
        aws cloudformation ${CMD}-stack \
            --stack-name ${STACK_NAME} \
            --template-body file://${STACK} \
            --capabilities CAPABILITY_NAMED_IAM \
            --region ${REGION}
        ;;
    delete)
        aws cloudformation delete-stack \
            --stack-name ${STACK_NAME} \
            --region ${REGION}
        ;;
    describe)
        aws cloudformation describe-stacks \
            --stack-name ${STACK_NAME} \
            --region ${REGION}
        ;;
    validate)
        aws cloudformation validate-template \
            --template-body file://${STACK} \
            --region ${REGION}
        ;;
    *)
        echo "Command not supported!"
        usage
        ;;
esac

exit 0
